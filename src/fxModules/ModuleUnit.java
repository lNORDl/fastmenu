package fxModules;

import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import org.json.simple.JSONObject;
import util.Util;

import java.awt.*;
import java.io.*;
import java.nio.file.FileSystems;

/**
 * Created by NDS on 08.12.2014.
 */
public class ModuleUnit extends BasicModule
{
    @FXML
    ImageView imageView_glass;
    @FXML
    ImageView imageView_icon;

    private File file;
    private String name;

    private String id;

    public static ModuleUnit create(Pane pane, int posX, int posY, String id)
    {
        ModuleUnit module = new ModuleUnit();
        module.id = id;
        module.initAsChildren(pane);
        module.rootPane.setLayoutX(posX);
        module.rootPane.setLayoutY(posY);

        return module;
    }

    public ModuleUnit()
    {
        resourseFxml = "assets/modules/mUnit_Form.fxml";
        resourseCss = Util.getPath("assets/css/UnitStyles.css");
        controller = this;
    }

    public String getId()
    {
        return id;
    }

    protected void onInited()
    {
        super.onInited();


        imageView_glass.setImage(new Image(Util.getPath("assets/images/ButtonUnitGlass.png")));
        imageView_glass.setViewport(new Rectangle2D(0, 0, 50, 50));

        rootPane.setOnMouseEntered((event)->
        {
            if(file != null) app.getModuleMain().showLabel(name);
            imageView_glass.setViewport(new Rectangle2D(50, 0, 50, 50));
        });

        rootPane.setOnMouseExited((event)->
        {
            if(file != null) app.getModuleMain().showLabel("");
            imageView_glass.setViewport(new Rectangle2D(0, 0, 50, 50));
        });

        rootPane.setOnMouseClicked((event)->
        {
            if(event.getButton() == MouseButton.PRIMARY)
            {
                launch();
            }

            if(event.getButton() == MouseButton.SECONDARY)
            {
                ModuleSelectContent.create(this);
            }
        });

        app.getModuleMain().addUnit(this);
        app.initUnitFromParametrs(this);
    }

    public void initUnit(File f)
    {
        file = f;

        if(file.isDirectory()) name = file.getAbsolutePath();
        else name = file.getName();

        Image icon = app.getFileIcon(file);
        imageView_icon.setImage(icon);

        JSONObject json = app.getParametrs();
        JSONObject units = (JSONObject) json.get("units");
        units.put(id, file.getAbsolutePath());

        app.saveParametrs();
    }

    private void launch()
    {
        if(file != null)
        {
            try
            {
                Desktop.getDesktop().open(file);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}