package fxModules;

import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import util.Util;

/**
 * Created by NDS on 08.12.2014.
 */
public class ModuleUnitsPanel extends BasicModule
{
    public static ModuleUnitsPanel create(Pane pane, String id)
    {
        ModuleUnitsPanel module = new ModuleUnitsPanel();
        module.id = id;
        module.initAsChildren(pane);

        return module;
    }

    private String id;

    public ModuleUnitsPanel()
    {
        resourseFxml = "assets/modules/mUnitsPanel_Form.fxml";
        resourseCss = Util.getPath("assets/css/UnitsPanelStyles.css");
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        ModuleUnit.create(rootPane, 10, 8, id + ":u1");
        ModuleUnit.create(rootPane, 10 + 50 + 14, 8, id + ":u2");
        ModuleUnit.create(rootPane, 10 + 50 + 14 + 50 + 14, 8, id + ":u3");
        ModuleUnit.create(rootPane, 10 + 50 + 14 + 50 + 14 + 50 + 14, 8, id + ":u4");

        rootPane.setOnScroll((event)->
        {
            double dY = event.getDeltaY();
            app.getModuleMain().rollPanels(dY);
        });
    }
}