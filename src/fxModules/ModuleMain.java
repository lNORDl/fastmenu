package fxModules;

import com.oracle.deploy.update.UpdateCheck;
import com.sun.net.httpserver.HttpServer;
import javafx.animation.Interpolator;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.*;
import org.json.simple.JSONObject;
import util.IDo;
import util.Util;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by NDS on 08.12.2014.
 */
public class ModuleMain extends BasicModule
{
    @FXML
    AnchorPane aPane_root;
    @FXML
    Button button_close;
    @FXML
    Button button_minimaize;
    @FXML
    Button button_move;
    @FXML
    Label label_name;
    @FXML
    VBox vBox_unitsPanels;
    @FXML
    ScrollPane scrollPane_unitsPanels;

    private int width = 306;
    private int height = 133;

    private int posY;

    private boolean isScroll;

    private boolean isMinimaize;

    private HashMap<String, ModuleUnit> units;

    private boolean isMove;

    private FileSystemView fileSystemView;
    private ArrayList<File> rootDirs;
    private ArrayList<Button> usbButtons;

    public ModuleMain()
    {
        moduleId = Util.ModuleID.M_MAIN;

        titleWindow = "Аудио-архив";

        resourseFxml = "assets/modules/mMain_Form.fxml";
        resourseCss = Util.getPath("assets/css/MainStyles.css");
        controller = this;

        units = new HashMap<String, ModuleUnit>();
        rootDirs = new ArrayList<File>();
        usbButtons = new ArrayList<Button>();

        fileSystemView = FileSystemView.getFileSystemView();
    }

    protected void onInited()
    {
        super.onInited();


        stage.setAlwaysOnTop(true);
        stage.getScene().setFill(null);

        posY = Integer.parseInt(app.getParametrs().get("position").toString());

        stage.setX(-width + 10);
        stage.setY(posY);

        label_name.setText("");

        aPane_root.setOnMouseClicked((event)->
        {
            if(isMinimaize) app.moveTo(aPane_root, width-10, 0, 200, Interpolator.EASE_BOTH);
        });

        aPane_root.setOnMouseEntered((event)->
        {
            if(!isMinimaize) app.moveTo(aPane_root, width-10, 0, 200, Interpolator.EASE_BOTH);
        });

        aPane_root.setOnMouseExited((event)->
        {
            int pos = isMinimaize?-9:0;
            app.moveTo(aPane_root, pos, 0, 200, Interpolator.EASE_BOTH);
        });

        initButtonSetting(button_close, 0, "Close", ()->
        {
            app.close();
        }, ()->
        {
            showLabel("Close");
        }, ()->
        {
            showLabel("");
        }, null, null);

        initButtonSetting(button_minimaize, 14, "Minimize", ()->
        {
            isMinimaize = !isMinimaize;
            showLabel("Minimaize: " + (isMinimaize?"On":"Off"));
        }, ()->
        {
            showLabel("Minimaize: " + (isMinimaize?"On":"Off"));
        }, ()->
        {
            showLabel("");
        }, null, null);

        initButtonSetting(button_move, 28, "Move", null, ()->
        {
            showLabel("Move");
        }, ()->
        {
            showLabel("");
        }, ()->
        {
            isMove = true;
        }, ()->
        {
            isMove = false;
            app.getParametrs().put("position", (int) stage.getY());
            app.saveParametrs();
        });

        button_move.setOnMouseDragged((event) ->
        {
            if (isMove) stage.setY(event.getScreenY() - button_move.getLayoutY() - 7);
        });


        ModuleUnitsPanel panel1 = ModuleUnitsPanel.create(vBox_unitsPanels, "p1");
        ModuleUnitsPanel panel2 = ModuleUnitsPanel.create(vBox_unitsPanels, "p2");

        initCheckUsb();
    }

    private void initCheckUsb()
    {
        Thread t = new Thread(()->
        {
            while (true)
            {
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }

                ArrayList<File> newRoot = new ArrayList<File>();
                ArrayList<File> files = new ArrayList<File>(Arrays.asList(File.listRoots()));

                boolean isUpdate = false;

                for(int i = 0; i < files.size(); i++)
                {
                    File file = files.get(i);

                    String d = fileSystemView.getSystemTypeDescription(file);
                    if(d.compareTo("Съемный диск") == 0)
                    {
                        newRoot.add(file);
                        if(rootDirs.indexOf(file) == -1) isUpdate = true;
                    }
                }

                for(int i = 0; i < rootDirs.size(); i++)
                {
                    if(newRoot.indexOf(rootDirs.get(i)) == -1)
                    {
                        isUpdate = true;
                        break;
                    }
                }

                rootDirs = newRoot;

                if(isUpdate)
                {
                    Platform.runLater(()->
                    {
                        updateUsbButtons();
                    });
                }
            }
        });
        t.start();
    }

    private void updateUsbButtons()
    {
        for(int i = 0; i < usbButtons.size(); i++)
        {
            aPane_root.getChildren().remove(usbButtons.get(i));
        }

        for (int i = 0; i < rootDirs.size(); i++)
        {
            if(i < 4) createUsbButton(rootDirs.get(i), i);
        }
    }

    private Button createUsbButton(File file, int n)
    {
        Button button = new Button("");
        button.setPrefWidth(24);
        button.setPrefHeight(24);
        button.getStyleClass().addAll("button-usb");

        // 65
        button.setLayoutX(48 + 65*n);
        button.setLayoutY(104);

        button.setOnMouseClicked((event)->
        {
            try
            {
                Desktop.getDesktop().open(file);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        });

        button.setOnMouseEntered((event)->
        {
            showLabel(fileSystemView.getSystemDisplayName(file));
        });

        button.setOnMouseExited((event)->
        {
            showLabel("");
        });

        aPane_root.getChildren().add(button);

        usbButtons.add(button);

        return button;
    }

    public void showLabel(String text)
    {
        label_name.setText(text);
    }

    public void rollPanels(double d)
    {
        isScroll = true;
        double t = 1 / 3;
        if(d > 0)
        {
            //scrollPane_unitsPanels.setVvalue(t);
            //scrollPane_unitsPanels.setVvalue((double) 66/132);
        }
        if(d < 0)
        {
            //System.out.println("zzz");
            //scrollPane_unitsPanels.setVvalue(t);
        }
    }

    public void addUnit(ModuleUnit unit)
    {
        units.put(unit.getId(), unit);
    }

    private void initButtonSetting(Button button, int x, String label, IDo onClick, IDo onMEntered, IDo onMExited, IDo onMPressed, IDo onMReleased)
    {
        ImageView iV;
        iV = new ImageView(Util.getPath("assets/images/ButtonSettingsIcons.png"));
        iV.setViewport(new Rectangle2D(x, 0, 14, 14));
        button.setGraphic(iV);

        button.setOnMouseClicked((event)->
        {
            if(onClick != null) onClick.doTask();
        });

        button.setOnMouseEntered((event)->
        {
            if(onMEntered != null) onMEntered.doTask();
        });

        button.setOnMouseExited((event)->
        {
            if(onMExited != null) onMExited.doTask();
        });

        button.setOnMousePressed((event)->
        {
            if(onMPressed != null) onMPressed.doTask();
        });

        button.setOnMouseReleased((event) ->
        {
            if (onMReleased != null) onMReleased.doTask();
        });
    }
}