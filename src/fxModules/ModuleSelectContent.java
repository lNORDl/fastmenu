package fxModules;

import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import org.json.simple.JSONObject;
import util.IDo;
import util.Util;

import java.io.File;
import java.io.IOException;

/**
 * Created by NDS on 08.12.2014.
 */
public class ModuleSelectContent extends BasicModule
{
    @FXML
    RadioButton radioButton_file;
    @FXML
    RadioButton radioButton_dir;
    @FXML
    TextField tField_path;
    @FXML
    Button button_select;
    @FXML
    Button button_save;

    private ModuleUnit unit;
    private File file;

    public static ModuleSelectContent create(ModuleUnit unit)
    {
        ModuleSelectContent module = new ModuleSelectContent();
        module.unit = unit;
        module.initAsFree();

        return module;
    }

    public ModuleSelectContent()
    {
        resourseFxml = "assets/modules/mSelectContent_Form.fxml";
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        button_select.setOnMouseClicked((event)->
        {
            if(radioButton_file.isSelected())
            {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Выбор файла");
                fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Files", "*"));
                file = fileChooser.showOpenDialog(stage);
            }

            if(radioButton_dir.isSelected())
            {
                DirectoryChooser dirChooser = new DirectoryChooser();
                dirChooser.setTitle("Выбор директории");
                file = dirChooser.showDialog(stage);
            }

            if(file != null) tField_path.setText(file.getAbsolutePath());
        });

        button_save.setOnMouseClicked((event)->
        {
            if(file != null)
            {
                unit.initUnit(file);
                destroy();
            }
        });
    }
}