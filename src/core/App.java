package core;

import fxModules.BasicModule;
import fxModules.ModuleMain;
import fxModules.ModuleUnit;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.json.simple.JSONObject;
import util.Util;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by NDS on 02.04.2015.
 */
public class App
{
    private static App instance;

    private Stage stage;
    private ArrayList<BasicModule> modules;

    private ModuleMain moduleMain;

    private JSONObject parametrs;

    public App(Stage s) throws IOException
    {
        instance = this;
        stage = s;
        modules = new ArrayList<BasicModule>();

        parametrs = Util.parseStringToJObject(Util.readFile("parametrs.txt"));

        moduleMain = new ModuleMain();
        moduleMain.initAsModal(stage, StageStyle.UNDECORATED);
    }

    public void moveTo(Node node, int x, int y, int delay, Interpolator i)
    {
        Timeline timeline = new Timeline();
        timeline.setCycleCount(1);
        timeline.setAutoReverse(true);
        KeyValue kv1 = new KeyValue(node.layoutXProperty(), x, i);
        KeyValue kv2 = new KeyValue(node.layoutYProperty(), y, i);
        KeyFrame kf = new KeyFrame(Duration.millis(delay), kv1, kv2);
        timeline.getKeyFrames().add(kf);
        timeline.play();
    }

    public Stage getStage()
    {
        return stage;
    }

    public ModuleMain getModuleMain()
    {
        return moduleMain;
    }

    public JSONObject getParametrs()
    {
        return parametrs;
    }

    public void saveParametrs()
    {
        Util.writeFile("parametrs.txt", parametrs.toJSONString());
    }

    public void initUnitFromParametrs(ModuleUnit unit)
    {
        String id = unit.getId();
        JSONObject units = (JSONObject) parametrs.get("units");
        String path = (String) units.get(id);
        if(path != null)
        {
            File file = new File(path);
            if(file.exists()) unit.initUnit(file);
        }
    }

    public void addModule(BasicModule module)
    {
        int i = modules.indexOf(module);
        if(i == -1) modules.add(module);
    }

    public void removeModule(BasicModule module)
    {
        int i = modules.indexOf(module);
        if(i != -1) modules.remove(i);
    }

    public BasicModule getModuleByID(Util.ModuleID id)
    {
        for(int i = 0; i < modules.size(); i++)
        {
            //System.out.println(modules.get(i).getId() + ":" + moduleId);
            if(modules.get(i).getModuleId() == id) return modules.get(i);
        }

        return null;
    }

    public Image getFileIcon(File file)
    {
        ImageIcon icon = null;

        try
        {
            sun.awt.shell.ShellFolder sf = sun.awt.shell.ShellFolder.getShellFolder(file);
            icon = new ImageIcon(sf.getIcon(true));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        int size = 32;
        BufferedImage bufferedImage = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
        bufferedImage.getGraphics().drawImage(icon.getImage().getScaledInstance(size, size, 0), 0, 0, null);
        Image image = SwingFXUtils.toFXImage(bufferedImage, null);

        return image;
    }

    public void setCenterPos(Pane pane, int x, int y)
    {
        pane.setLayoutX(x - pane.getPrefWidth() / 2);
        pane.setLayoutY(y - pane.getPrefHeight() / 2);
    }

    public String getPath(String p)
    {
        return new File(p).toURI().toString();
    }

    public void close()
    {
        for(int i = 0; i < modules.size(); i++)
        {
            modules.get(i).destroy();
        }
        stage.close();
        Platform.exit();
        System.exit(0);
    }

    public static App getInstance()
    {
        return instance;
    }
}
