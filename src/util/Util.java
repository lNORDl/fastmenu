package util;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by NDS on 17.01.2015.
 */
public class Util
{
    public static enum ModuleID {M_NONE, M_MAIN, M_PLAYER, M_LOGIN, M_LOADER, M_ADD_AUDIOTRACK, M_AUDIO_ARCHIVE};

    public static HashMap<String, Object> config = new HashMap<String, Object>();

    public static void init()
    {
        config.put("host", "localhost:3000");
        config.put("download", "download");
        config.put("ACCESS_ADMIN", 3);
        config.put("ACCESS_USER", 1);
    }

    public static String getPath(String p)
    {
        return new File(p).toURI().toString();
    }

    public static HBox getHBox(int spacing)
    {
        HBox hBox = new HBox();
        hBox.setSpacing(spacing);
        hBox.setPrefWidth(Region.USE_COMPUTED_SIZE);
        hBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
        hBox.setMaxWidth(Double.MAX_VALUE);
        hBox.setMaxHeight(Double.MAX_VALUE);

        return hBox;
    }

    public static int getRandomInt(int min, int max) {

        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static String readFile(String path)
    {
        File file = new File(path);
        try {
            String contents = new Scanner(file).useDelimiter("\\Z").next();
            return contents;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void writeFile(String path, String content)
    {
        PrintWriter writer = null;
        try
        {
            writer = new PrintWriter(path, "UTF-8");
            writer.print(content);
            writer.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
    }

    public static JSONObject parseStringToJObject(String s)
    {
        JSONObject json = null;
        try {
            json = (JSONObject) new JSONParser().parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }

    public static JSONArray parseStringToJArray(String s)
    {
        JSONArray json = null;
        try {
            json = (JSONArray) new JSONParser().parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }
}
